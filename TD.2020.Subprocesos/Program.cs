﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TD._2020.Subprocesos
{

  
    class Program
    {
        
        static int i = 65;
        static void Main(string[] args)
        {
            var taskA = Task.Factory.StartNew(metodoA);
            var taskB = Task.Factory.StartNew(metodoB);
           
            Task.WaitAll(new Task[] { taskA, taskB });

            Console.WriteLine("Presione cualquier tecla para salir");
            Console.ReadLine();



            Console.ReadKey();

        }

        private static void metodoA()
        {
            for (; i < 91;)
            {
                if ((i % 2) == 1 )
                {
                    Console.WriteLine(Convert.ToChar(i) + " " + System.Threading.Thread.CurrentThread.ManagedThreadId);
                 
                    i++;
                }
            }
        }

        private static void metodoB()
        {

            for (; i < 91;)
            {
                if ((i % 2) == 0)
                {
                    Console.WriteLine(Convert.ToChar(i) + " " + System.Threading.Thread.CurrentThread.ManagedThreadId);
                   
                    i++;
                }
            }
        }










        void EjemploJoin()
        {

            Thread ta = new Thread(delegate ()
            {

                for (int i = 0; i < 100; i++)
                {
                    Console.WriteLine($"item T1 {i}");
                }


            });

            Thread tb = new Thread(delegate ()
            {

                for (int i = 2000; i < 2100; i++)
                {
                    Console.WriteLine($"item T2 {i}");
                }


            });



            tb.Start();
            tb.Join();
            ta.Start();

        }
        public class Singleton
        {
            public string Name;
            static Singleton _instance;
            private Singleton() { }


            static object _lock = new object();
            public static Singleton GetInstance
            {
                get
                {
                    // lock (_lock)
                    {
                        if (_instance == null)
                        {
                            _instance = new Singleton();
                            _instance.Name = $"{Thread.CurrentThread.Name}";
                        }

                    }

                    return _instance;
                }
            }
        }
        void EjemploBloqueo()
        {
            Thread t1 = new Thread(delegate ()
            {

                Console.WriteLine($"T1 => {Singleton.GetInstance.Name}");
            });
            t1.Name = "thread 1";
            t1.Priority = ThreadPriority.AboveNormal;


            Thread t2 = new Thread(delegate ()
            {

                Console.WriteLine($"T2 => {Singleton.GetInstance.Name}");

            });
            t2.Name = "thread 2";
            t2.Priority = ThreadPriority.AboveNormal;


            Thread t3 = new Thread(delegate ()
            {

                Console.WriteLine($"T3 => {Singleton.GetInstance.Name}");
            });
            t3.Name = "thread 3";
            t3.Priority = ThreadPriority.Highest;

            t1.Start();
            t2.Start();
            t3.Start();




            Console.ReadKey();
        }
        void EjemploParalelismo()
        {
            int sleep = 10;
            int degree = 20;
            int i = 0;
            long tick1 = Environment.TickCount;
            for (i = 0; i < 1000; i++)
            {
                // Thread.Sleep(sleep);
            }
            long tick2 = Environment.TickCount - tick1;



            Console.WriteLine($"ticks sin paralelismo=>{tick2}");

            tick1 = Environment.TickCount;
            Parallel.For(0, 1000, new ParallelOptions() { MaxDegreeOfParallelism = degree }, ip =>
            {
                Thread.Sleep(sleep);
            });
            tick2 = Environment.TickCount - tick1;
            Console.WriteLine($"ticks con paralelismo de grado {degree}=>{tick2}");



        }
        void Ejemplo1()
        {
            //depurar subprocesos
            bool end = false;

            long c1 = 0, c2 = 0, c3 = 0;

            Thread t1 = new Thread(delegate ()
            {

                while (!end)
                {
                    c1++;
                }


            });
            t1.Name = "t1";
            t1.Priority = ThreadPriority.BelowNormal;


            Thread t2 = new Thread(delegate ()
            {

                while (!end)
                {
                    c2++;
                }



            });
            t2.Name = "t2";
            t2.Priority = ThreadPriority.Normal;


            Thread t3 = new Thread(delegate ()
            {

                while (!end)
                {
                    c3++;
                }



            });
            t3.Name = "t3";
            t3.Priority = ThreadPriority.Highest;

            t1.Start();
            t2.Start();
            t3.Start();


            Thread.Sleep(1000);
            end = true;

            Console.WriteLine($"T1=>{c1}, T2=> {c2}. T3=>{c3}");
        }

    }
}
